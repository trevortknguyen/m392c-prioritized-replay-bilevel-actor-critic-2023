# Hide pygame support prompt
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'


from gym.envs.registration import register

# merge_env.py
register(
    id='merge-v0',
    entry_point='highway_env.envs:MergeEnv',
)
