import setuptools

setuptools.setup(
    name='prbiac',
    version='0.0.1',
    packages=setuptools.find_packages(),
    install_requires=[
        'gym==0.19.0',
        'tensorflow',
        'python-dateutil',
        'tabulate',
        'matplotlib',
        'scipy',
        'tensorboardX',
        'pandas',
        'pygame',
    ],
)
