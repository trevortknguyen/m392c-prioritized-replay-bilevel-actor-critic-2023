# prioritized replay bilevel actor critic

## dependencies installing

```
pip install --editable .
```

## plotting

```
python -m plot_proportions
```

## running experiments

```
python -m run_pr_bilevel
python -m run_pr_maddpg
```

## acknowledgements

The code in `bilevel_pg` is from https://github.com/laonahongchen/Bilevel-Optimization-in-Coordination-Game/.

The code in `highway_env` is from https://github.com/eleurent/highway-env.