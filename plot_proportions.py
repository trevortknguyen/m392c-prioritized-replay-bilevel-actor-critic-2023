import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

bilevel_csv_path='./bilevel_20230310-164205_full/progress.csv'
maddpg_csv_path='./maddpg_20230306-111521_full/progress.csv'
 
# bilevel_csv_path='./pr_bilevel_20230319-231831_full/progress.csv'
# maddpg_csv_path='./pr_maddpg_20230319-232014_full/progress.csv'

def etl_data(filename, episode_bin_size, seed=None):
    df = pd.read_csv(filename, usecols=['episodes', 'last-path-return_agent_0', 'last-path-return_agent_1'])

    if seed is not None:
        seed_start = 8000 * seed
        seed_end = 8000 * (seed + 1)
        df = df.iloc[seed_start:seed_end]

    def determine_status(reward0, reward1):
        if reward0 == 50 and reward1 == 10:
            return 'leader'
        elif reward0 == 10 and reward1 == 50:
            return 'follower'
        elif reward0 < 0 and reward1 < 0:
            return 'crash'
        else:
            raise ValueError()

    df['result'] = df.apply(lambda row: determine_status(row['last-path-return_agent_0'], row['last-path-return_agent_1']), axis=1)

    # assign bins by integer division
    df['bin_id'] = (df['episodes'] - 1) // episode_bin_size
    gb = df.groupby('bin_id')['result'].value_counts()

    # ensure proper ordering
    data = gb.unstack().fillna(0).reindex(columns=['follower', 'crash', 'leader']).sort_index(axis='index').values.T
    return data

def plot_data(ax, data, title, n_episodes, episode_bin_size):
    # calculate cumulative proportions
    cum_data = np.cumsum(data / np.sum(data, axis=0), axis=0)

    # create plot
    ax.set_xlim([0, n_episodes- episode_bin_size])
    ax.set_ylim([0.0, 1.0])
    # ax.set_xlabel("Episode")
    # ax.set_ylabel("Rate")
    ax.fill_between(np.arange(0, n_episodes, episode_bin_size), 0, cum_data[0], color="wheat", label="Follower go first")
    ax.fill_between(np.arange(0, n_episodes, episode_bin_size), cum_data[0], cum_data[1], color="lightpink", label="Crash")
    ax.fill_between(np.arange(0, n_episodes, episode_bin_size), cum_data[1], cum_data[2], color="lightgreen", label="Leader go first")
    # ax.set_title(title)
    # ax.legend()
    return ax

n_episodes = 8000
bin_size = 100

fig = plt.figure(constrained_layout=True)
fig.suptitle('Baseline')

subfigs = fig.subfigures(nrows=1, ncols=2)
subfigs[0].suptitle('bilevel')
subfigs[1].suptitle('maddpg')

ax = [subfig.subplots(nrows=10, ncols=1) for subfig in subfigs]

for seed_row in range(10):
    plot_data(
        ax=ax[0][seed_row],
        data=etl_data(
            filename=bilevel_csv_path,
            episode_bin_size=bin_size,
            seed=seed_row,
        ),
        title='bilevel',
        n_episodes=n_episodes,
        episode_bin_size=bin_size,
    )

    plot_data(
        ax=ax[1][seed_row],
        data=etl_data(
            filename=maddpg_csv_path,
            episode_bin_size=bin_size,
            seed=seed_row,
        ),
        title='maddpg',
        n_episodes=n_episodes,
        episode_bin_size=bin_size,
    )

plt.show()
